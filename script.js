/* 
1. What is the term given to unorganized code that's very hard to work with?
spaghetti code
2. How are object literals written in JS?
{}
3. What do you call the concept of organizing information and functionality to belong to an object?
object-oriented programming
4. If studentOne has a method named enroll(), how would you invoke it?
studentOne.enroll()
5. True or False: Objects can have objects as properties.
true
6. What is the syntax in creating key-value pairs?
const studen = {
    key: value
}
7. True or False: A method can have no parameters and still work.
true
8. True or False: Arrays can have objects as elements.
true
9. True or False: Arrays are objects.
true
10. True or False: Objects can have arrays as properties.
true
*/


//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.
//For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90,
//false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo()
//that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc()
//that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89,84,78,88],
    computeGrades: function(){
        let sum = 0;
        this.grades.forEach(function(grade){
            sum += grade
        })
        return sum/this.grades.length
    },
    willPass: function(){
        if (this.computeGrades() >=85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        if (this.computeGrades() >=90){
            return true
        } else if (this.computeGrades() >=85){
            return false
        } else {
            return undefined
        }
    }
}
studentOne.computeGrades()
console.log(studentOne.computeGrades())
console.log(studentOne.willPass())

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78,82,79,85],
    computeGrades: function(){
        let sum = 0;
        this.grades.forEach(function(grade){
            sum += grade
        })
        return sum/this.grades.length
    },
    willPass: function(){
        if (this.computeGrades() >=85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        if (this.computeGrades() >=90){
            return true
        } else if (this.computeGrades() >=85){
            return false
        } else {
            return undefined
        }
    }
}
studentTwo.computeGrades()
console.log(studentTwo.computeGrades())


let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87,89,91,93],
    computeGrades: function(){
        let sum = 0;
        this.grades.forEach(function(grade){
            sum += grade
        })
        return sum/this.grades.length
    },
    willPass: function(){
        if (this.computeGrades() >=85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        if (this.computeGrades() >=90){
            return true
        } else if (this.computeGrades() >=85){
            return false
        } else {
            return undefined
        }
    }
}
studentThree.computeGrades()
console.log(studentThree.computeGrades())


let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91,89,92,93],
    computeGrades: function(){
        let sum = 0;
        this.grades.forEach(function(grade){
            sum += grade
        })
        return sum/this.grades.length
    },
    willPass: function(){
        if (this.computeGrades() >=85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        if (this.computeGrades() >=90){
            return true
        } else if (this.computeGrades() >=85){
            return false
        } else {
            return undefined
        }
    }
}
studentFour.computeGrades()
console.log(studentFour.computeGrades())


let classOf1A = {
    students: [studentOne,studentTwo,studentThree,studentFour],
    countHonorStudents: function(){
        let totalHonorStudents = 0
        this.students.forEach(function(student){
            if (student.willPassWithHonors()){
                totalHonorStudents ++ 
            }
        })
        return totalHonorStudents
    },
    honorPercentage: function(){
        return (this.countHonorStudents()/this.students.length)*100
    },
    retrieveHonorStudentsInfo: function(){
        return this.students.filter(function(student){
            return student.willPassWithHonors
        }).map(function(student){
            return {
                email: student.email,
                averageGrade: student.computeGrades()
            }
        })
    },
    sortHonorStudentsByGradeDesc: function(){
        return this.retrieveHonorStudentsInfo().sort(function(a,b){
            return b.averageGrade - a.averageGrade
        })
    }
}
